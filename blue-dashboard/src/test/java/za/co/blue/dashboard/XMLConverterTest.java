/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.blue.dashboard;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import za.co.blue.dashboard.dto.AuthRequest;
import za.co.blue.dashboard.dto.Event;

/**
 *
 * @author raghunandan
 */
public class XMLConverterTest {

    public XMLConverterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testXMLConversion() throws PropertyException, JAXBException {

        JAXBContext context = JAXBContext.newInstance(AuthRequest.class);

        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

        AuthRequest request = new AuthRequest();
        request.setEventType("Authentication");

        Event event = new Event();
        event.setDeviceId("12345");
        event.setDeviceSer("ABCDE");
        event.setDeviceVer("ABCDE");
        event.setTransType("Users");
        event.setUserPin("12345");

        request.setEvent(event);

        m.marshal(request, System.out);

    }
}
