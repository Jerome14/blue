/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.blue.dashboard.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class represents an Event.
 *
 * @author raghunandan
 */
@XmlType(propOrder = {"userPin", "deviceId", "deviceSer", "deviceVer", "transType"})
public class Event {

    private String userPin;

    private String deviceId;

    private String deviceSer;

    private String deviceVer;

    private String transType;

    public Event() {
    }

    @XmlElement(name = "UserPin")
    public String getUserPin() {
        return userPin;
    }

    public void setUserPin(String userPin) {
        this.userPin = userPin;
    }

    @XmlElement(name = "DeviceId")
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @XmlElement(name = "DeviceSer")
    public String getDeviceSer() {
        return deviceSer;
    }

    public void setDeviceSer(String deviceSer) {
        this.deviceSer = deviceSer;
    }

    @XmlElement(name = "DeviceVer")
    public String getDeviceVer() {
        return deviceVer;
    }

    public void setDeviceVer(String deviceVer) {
        this.deviceVer = deviceVer;
    }

    @XmlElement(name = "TransType")
    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    @Override
    public String toString() {
        return "Event{" + "userPin=" + userPin + ", deviceId=" + deviceId + ", deviceSer=" + deviceSer + ", deviceVer=" + deviceVer + ", transType=" + transType + '}';
    }

}
