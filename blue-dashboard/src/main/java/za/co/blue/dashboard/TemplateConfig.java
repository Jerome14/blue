/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.blue.dashboard;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

/**
 * This class configures the Thymeleaf template engine.
 *
 * @author raghunandan
 */
@Configuration
public class TemplateConfig {

    @Bean
    public TemplateEngine getWifireTemplateEngine() {
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.addTemplateResolver(getClassLoaderTemplateResolver());
        return templateEngine;
    }

    @Bean(name = "ClassLoaderTemplateResolver")
    public TemplateResolver getClassLoaderTemplateResolver() {
        TemplateResolver templateResolver = new ClassLoaderTemplateResolver();

        templateResolver.setTemplateMode("XHTML");
        templateResolver.setPrefix("templates/");
        templateResolver.setOrder(1);
        templateResolver.setCacheTTLMs(0L);

        return templateResolver;
    }
}
