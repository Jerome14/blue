package za.co.blue.dashboard;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

/**
 * This class will be used to initialise the servlet and filter mappings when
 * the application is deployed as a war file. This class is not used when the
 * application is build as a jar file.
 *
 * @author raghunandan
 */
public class WebInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }
}
