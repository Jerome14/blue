/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.blue.dashboard.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class represents the Authentication request. JAXB is used to convert
 * objects of this class into XML.
 *
 * @author raghunandan
 */
@XmlRootElement(name = "request")
@XmlType(propOrder = {"eventType", "event"})
public class AuthRequest {

    private String eventType;

    private Event event;

    public AuthRequest() {
    }

    @XmlElement(name = "EventType")
    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @XmlElement(name = "event")
    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return "AuthRequest{" + "eventType=" + eventType + ", event=" + event + '}';
    }

}
