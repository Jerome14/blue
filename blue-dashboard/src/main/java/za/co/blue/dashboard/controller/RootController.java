/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.blue.dashboard.controller;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import za.co.blue.dashboard.service.AuthenticationService;

/**
 * This is the root controller for the Blue Dashboard.
 *
 * @author raghunandan
 */
@Controller
@RequestMapping("")
public class RootController {

    @Value("${blue.server.hostname}")
    private String hostName;

    @Value("${blue.server.port}")
    private String port;

    @Autowired
    private AuthenticationService authService;

    /**
     * This method redirects to the dashboard page.
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView getRootPage(HttpServletRequest request) {

        try {

            Map<String, String> map = initDashboardFields();

            return new ModelAndView("dashboard", map);

        } catch (Exception e) {
            e.printStackTrace();
            return new ModelAndView("500");
        }
    }

    /**
     * This is the POST method that is used to send the XML message to the
     * server.
     *
     * @param hostName
     * @param port
     * @param xmlMessage
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ModelAndView connectToServer(
            @RequestParam(value = "hostName", required = true) String hostName,
            @RequestParam(value = "port", required = true) String port,
            @RequestParam(value = "xmlMessage", required = true) String xmlMessage) {

        System.out.println("Connecting to server...");

        try {

            String response = authService.authenticate(hostName, port, xmlMessage);

            Map<String, String> map = initDashboardFields();
            map.put("serverResponse", response);

            return new ModelAndView("dashboard", map);
        } catch (Exception e) {
            e.printStackTrace();
            return new ModelAndView("500");
        }
    }

    /**
     * This method initialises the model for the dashboard page.
     *
     * @return
     */
    private Map<String, String> initDashboardFields() {
        Map<String, String> map = new HashMap<>();
        map.put("hostName", hostName);
        map.put("port", port);
        map.put("serverResponse", "");

        boolean includeXmlHeader = false;
        String xmlMessage = authService.buildAuthRequest(includeXmlHeader);

        map.put("xmlMessage", xmlMessage);

        return map;
    }

}
