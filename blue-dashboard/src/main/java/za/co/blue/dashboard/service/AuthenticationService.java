/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.blue.dashboard.service;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.Socket;
import javax.transaction.Transactional;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.springframework.stereotype.Service;
import za.co.blue.dashboard.dto.AuthRequest;
import za.co.blue.dashboard.dto.Event;

/**
 * The Authentication Service provides useful methods required for
 * authentication.
 *
 * @author raghunandan
 */
@Service
public class AuthenticationService {

    private static final String DEFAULT_CHARSET = "UTF-8";
    private static final int DEFAULT_BUFFER_SIZE = 8192;

    /**
     * This method creates a socket connection to the host on the given port and
     * sends the xml message through the socket. The response from the server is
     * returned.
     *
     * @param hostName
     * @param port
     * @param xmlMessage
     * @return
     */
    @Transactional
    public String authenticate(String hostName, String port, String xmlMessage) {

        Socket clientSocket = null;
        BufferedWriter out = null;
        InputStream is = null;
        String serverResponse = null;

        try {
            clientSocket = new Socket(hostName, Integer.valueOf(port));

            String authRequest = xmlMessage;
            System.out.println("Client: " + authRequest);

            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream(), DEFAULT_CHARSET));
            out.write(authRequest);
            out.flush();

            clientSocket.shutdownOutput();

            is = clientSocket.getInputStream();

            serverResponse = getInputAsString(is);

            System.out.println("Server: " + serverResponse);

        } catch (Exception e) {
            e.printStackTrace();
            serverResponse = e.getMessage();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (clientSocket != null) {
                try {
                    System.out.println("Client: closing connection");
                    clientSocket.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return serverResponse;
    }

    /**
     * Return the default xml message.
     *
     * @param isXmlHeaderRequired - used to determine of the xml header is
     * required in the output, ie:
     * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
     * @return
     */
    @Transactional
    public String buildAuthRequest(boolean isXmlHeaderRequired) {

        String result = "";
        try {
            JAXBContext context = JAXBContext.newInstance(AuthRequest.class);

            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            if (!isXmlHeaderRequired) {
                m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            }

            AuthRequest request = new AuthRequest();
            request.setEventType("Authentication");

            Event event = new Event();
            event.setDeviceId("12345");
            event.setDeviceSer("ABCDE");
            event.setDeviceVer("ABCDE");
            event.setTransType("Users");
            event.setUserPin("12345");

            request.setEvent(event);

            StringWriter sw = new StringWriter();
            m.marshal(request, sw);

            result = sw.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Convert the data from the input stream into a string.
     *
     * @param is
     * @return
     * @throws IOException
     */
    @Transactional
    private String getInputAsString(InputStream is) throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        int count = 0;

        while ((count = is.read(buffer)) != -1) {
            baos.write(buffer, 0, count);
        }

        String result = baos.toString(DEFAULT_CHARSET);

        return result;
    }

}
