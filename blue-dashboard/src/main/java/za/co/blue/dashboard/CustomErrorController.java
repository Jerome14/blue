package za.co.blue.dashboard;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * This is the global error controller. Users will be redirected to the error
 * page in the case of a system error.
 *
 * @author raghunandan
 */
@RestController
public class CustomErrorController implements ErrorController {

    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public ModelAndView error() {
        return new ModelAndView("500");
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
