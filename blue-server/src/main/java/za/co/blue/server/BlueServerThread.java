/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.blue.server;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This thread processes the input from the socket and returns an
 * acknowledgment.
 *
 * @author raghunandan
 */
public class BlueServerThread extends Thread {

    private static final String DEFAULT_CHARSET = "UTF-8";
    private static final int DEFAULT_BUFFER_SIZE = 8192;

    private Socket socket = null;

    public BlueServerThread(Socket socket) {
        super("BlueServerThread");
        this.socket = socket;
    }

    @Override
    public void run() {

        OutputStream out = null;
        InputStream is = null;

        try {

            is = socket.getInputStream();

            String input = getInputAsString(is);
            System.out.println("Client: " + input);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
            String date = sdf.format(new Date());
            String outputMessage = "Authentication request received at: " + date;
            System.out.println("Server: " + outputMessage);

            out = new BufferedOutputStream(socket.getOutputStream());
            out.write(outputMessage.getBytes(DEFAULT_CHARSET));
            out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    System.out.println("Server: closing connection.");
                    socket.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Convert the data from the input stream into a string.
     *
     * @param is
     * @return
     * @throws IOException
     */
    private String getInputAsString(InputStream is) throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        int count = 0;

        while ((count = is.read(buffer)) != -1) {
            baos.write(buffer, 0, count);
        }

        String result = baos.toString(DEFAULT_CHARSET);

        return result;
    }
}
