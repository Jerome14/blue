/**
 * Copyright Interactive Solutions
 */
package za.co.blue.server;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This Global Exception Handler allows you to use the same exception handling
 * techniques across the whole application.
 *
 * @author raghunandan
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler {

    private final Logger log = Logger.getLogger(this.getClass());

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleException(IllegalArgumentException ex) {
        log.error(ex.getMessage(), ex);
        return "500";
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleInternalServerException(Exception ex) {
        log.error(ex.getMessage(), ex);
        return "500";
    }

}
