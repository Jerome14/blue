package za.co.blue.server;

import java.net.ServerSocket;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * This class is used to configure the application context and launch the
 * application.
 *
 * @author raghunandan
 */
@EnableAutoConfiguration
@EnableWebMvc
@Configuration
@ComponentScan
public class Application extends WebMvcConfigurerAdapter {

    /**
     * This method launches the application and starts a server listening on
     * port 9011.
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        try {

            ServerSocket serverSocket = new ServerSocket(9011);
            System.out.println("Blue Server listening on port 9011...");

            while (true) {
                new BlueServerThread(serverSocket.accept()).start();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
